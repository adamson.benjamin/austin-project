## Running the program

Execute `./compile.bash` to first compile the program (make) and then execute the
resulting reader/writer binaries as seperate processes. `run.bash` is a convenience script that
runs both processes from the terminal, but the output from the reader process can get interleaved
with the output from the writer process so this is not recommended. Instead it is recommended to run
each binary from a seperate terminal.

## Switching from Named Pipes to Shared Memory
Switching from my original implementation using named pipes to shared memory increased the throughput by 10x. The increase in performance comes from removing the system calls the original implementation used, instead handling the message passing and synchronization within the processes themselves.

The new implementation targets mean latency of each message, and it does so by sacrificing throughput of all the messages. An atomic boolean flag is shared between the reader/writer processes, allocated at the beginning of the shared memory block the processes share. As each process runs they atomically toggle the flag back and forth between reads and writes. This setup minimizes latency between when a message is written and when it is received. The program checks that the atomics are address-free, therefore free to be shared between processes (atleast on x86).

## Analysis
**Average mean : 158 ns**

**Average stdev: 119 ns**

**Number outliers: 4087 (.004%)**

The average mean trends smaller as I increase the number of messages from 1 million -> 10 million ->100 million -> 1 billion, but no more than 5%. For example my average mean for 1 billion messages is 155 ns.

## Outliers

![](data/averages.png)

The data set has a minimal number of outliers, out of 1 million only 0.004% of the data was outside 3 stdev of the mean latency. Using libreoffice to graph the data set I was unable to graph all one million points, it's too many for the application. I think the numbers do a better job of representing the data then the graph does, but people like graphs. ¯\_(ツ)_/¯

### Minimizing max of outliers
To reduce the max of the outliers I would investigate priming the CPU, sending a set number of messages without timing them before I started timing the one million messages I was attempting to optimize. I noticed manually reviewing the latencies that the outliers are more clustered towards the beginning of each trial. By priming the CPU it can better predict the code paths executed during the actual trial.

Beyond the priming I would spend time trying to optimize the OS environment itself, removing unnecessary processes (YouTube) and investigate adjusting process priorities in the kernel. Taking it a step further I could remove two cores from the overall scheduling algorithm and dedicate them to running the reader/writer processes.

## Compiler versions used
Both clang and gcc were used, both compiled from source within the last few days.
  * g++ (GCC) 12.0.0 20210710 (experimental)
  * clang version 13.0.0 (https://github.com/llvm/llvm-project.git d9659bf6a036545125a39648b4abe838080299ec)
