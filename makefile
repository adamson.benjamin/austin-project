CC     = g++
#CC     = clang++

OPTIMIZE = -Ofast
#OPTIMIZE = -g

CFLAGS = -Wall -std=c++20 -pedantic $(OPTIMIZE)

SHARED_TLU = src/shared.cxx

all: reader writer

writer:
	$(CC) $(CFLAGS) -o writer src/writer.cxx $(SHARED_TLU)

reader:
	$(CC) $(CFLAGS) -o reader src/reader.cxx $(SHARED_TLU)

clean: 
	$(RM) bin/writer bin/reader
