echo "############################################################################################"
echo "NOTE: Output of processes will be interleaved from both processes writing to STDOUT/STDERR.\n"
echo "Run reader/writer from seperate terminals to avoid this."
echo "############################################################################################"

./bin/reader &
./bin/writer
