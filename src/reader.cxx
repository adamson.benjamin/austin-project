#include "shared.hpp"
#include <algorithm>
#include <cmath>
#include <cstring>
#include <fstream>
#include <iostream>
#include <numeric>
#include <vector>

#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/shm.h>

namespace sh = ::shared;

using Averages = std::vector<int64_t>;

namespace
{
void
compute_timing_statistics(Averages const& averages)
{
  using Timing = typename Averages::value_type;
  sh::exit_if_error(averages.size() < 2, "Not enough messages to compute statistics");

  // Compute the mean latency for all messsage timings 
  long int const mean = [&]() {
    Timing const sum =  std::accumulate(std::cbegin(averages), std::cend(averages), 0LL);
    return sum / averages.size();
  }();

  // Compute the stdev latency for all messsage timings 
  Timing accum = 0;
  std::for_each(std::cbegin(averages), std::cend(averages), [&](auto const& d) {
    auto const         distance = d - mean;
    auto const squared_distance = distance * distance;
    accum                      += squared_distance;
  });

  Timing const stdev = std::sqrt(accum / (averages.size()-1));

  // This computation ignores outliers that are under the mean latency, those are not useful to
  // analyze.
  auto outliers = 0L;
  std::for_each(std::cbegin(averages), std::cend(averages), [&](auto const& value) {
      if (value > (mean + (3 * stdev))) {
        ++outliers;
      }
  });

  ::fprintf(stdout, "READER -- num outliers:  %ld\n", outliers);
  ::fprintf(stdout, "READER -- mean  latency: %ld\n", mean);
  ::fprintf(stdout, "READER -- stdev latency: %ld\n", stdev);
}

void
export_to_csv(Averages const& avgs)
{
  std::ofstream ofile;
  ofile.open("./averages.csv");
  for (auto const& a : avgs) {
    ofile << a;
    ofile << "\n";
  }
}

void
wait_for_all_messages_to_arrive(SharedMemory& sm, Averages& averages, int &count)
{
  ::fprintf(stdout, "READER -- Waiting for %d messages to arrive\n", sh::NUM_MESSAGES);
  while (count < sh::NUM_MESSAGES) {
    while (!sm.ready_for_read()) {}
    auto msg = sm.read_message();

    // Uncomment to validate messages meet criteria 
    //std::cerr << "TYPE: '" << (int)copy.type << "' time: '" << copy.send_t << "' payload: '" << copy.payload << "'\n";

    {
      // Compute difference between "now" and messages send_t.
      auto const recv_time = shared::nanos_since_epoch();
      auto const dt = recv_time - msg.send_t;
      averages.push_back(dt);
    }
    ++count;
  }
  ::fprintf(stdout, "READER -- all messages read, performing computations ..\n");
}

/// This process will send a message to the writer process, indicating this process is ready to
/// receive messages.
void
send_message_to_writer_process()
{
  auto const qid = sh::msgqueue_key();

  Message msg;
  sh::exit_if_error(0 != ::msgsnd(qid, &msg, sizeof(msg), 0), "msgsnd");
}

} // namespace

int
main(int argc, char **argv)
{
  ::fprintf(stdout, "READER -- starting\n");
  send_message_to_writer_process();

  Averages averages;
  averages.reserve(sh::NUM_MESSAGES);

  auto sm = SharedMemory::get_or_abort();
  {
    auto count = 0;
    wait_for_all_messages_to_arrive(sm, averages, count);
  }

  ::fprintf(stdout, "READER -- computing statistics for %lu messages\n", averages.size());
  compute_timing_statistics(averages);
  export_to_csv(averages);

  ::fprintf(stdout, "READER -- finished.\n");
  return EXIT_SUCCESS;
}
