#include "shared.hpp"

#include <atomic>
#include <chrono>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <fstream>

#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/shm.h>

namespace
{
/// WRITE_NEXT is chosen to be 'false' because the shared memory is all set to 0 (false) and the
/// first operation we want the two programs to perform is a write. By setting WRITE_NEXT to false
/// we ensure that a write happens initially.
auto constexpr READ_NEXT  = true;
auto constexpr WRITE_NEXT = false;
} // namespace

using flag_t = bool;
namespace sh = ::shared;

SharedMemory
SharedMemory::get_or_abort()
{
  auto const key = ::ftok(".", 117);
  sh::exit_if_error(-1 == key, "could not get key to shared memory");

  // Allocate exactly enough space for or flag and a single Message.
  auto constexpr BYTES_TO_ALLOCATE = sizeof(flag_t) + sizeof(Message);

  auto const id = ::shmget(key, BYTES_TO_ALLOCATE, IPC_CREAT | 0666);
  sh::exit_if_error(-1 == id, "allocating shared memory");

  void *psm = ::shmat(id, nullptr, 0);
  sh::exit_if_error(reinterpret_cast<void*>(-1) == psm, "mapping shared memory");
  std::memset(psm, 0, BYTES_TO_ALLOCATE);
  return SharedMemory{id, psm};
}

SharedMemory::SharedMemory(int const id, void* pmem)
  : id_{id}
  , pmem_{pmem}
  , pmessages_{static_cast<char*>(pmem_) + sizeof(flag_t)}
  , pflag_{new(pmem_) bool{false}}
{
  sh::exit_if_error(!std::atomic_ref<bool>{*pflag_}.is_lock_free(),
                    "in practice atomic must be lock free to be shared between processes.");
}

SharedMemory::~SharedMemory()
{
  sh::exit_if_error(0  != ::shmdt(pmem_), "detaching shared memory");
  sh::exit_if_error(-1 != ::msgctl(id_, IPC_RMID, nullptr), "msgctl");
}

Message&
SharedMemory::message_ref()
{
  return *static_cast<Message*>(pmessages_);
}

bool
SharedMemory::ready_for_read() const
{
  std::atomic_ref<bool> flag{flag_ref()};
  return flag == READ_NEXT;
}

bool
SharedMemory::ready_for_write() const
{
  std::atomic_ref<bool> flag{flag_ref()};
  return flag == WRITE_NEXT;
}

Message
SharedMemory::read_message()
{
  auto copy = message_ref();

  std::atomic_ref<bool> flag{flag_ref()};
  flag = WRITE_NEXT;

  return copy;
}

void
SharedMemory::write_message(Message const& m)
{
  message_ref() = m;

  std::atomic_ref<bool> flag{flag_ref()};
  flag = READ_NEXT;
}

namespace shared
{
int
msgqueue_key()
{
  auto const key = ::ftok(".", 117);
  exit_if_error(-1 == key, "msgqueue_key ftok");

  auto const qid = ::msgget(key, 0666 | IPC_CREAT);
  exit_if_error(-1 == qid, "msgget");
  return qid;
}

uint64_t
nanos_since_epoch()
{
  using namespace std::chrono;
  auto const delta =  system_clock::now().time_since_epoch();
  auto const    ns = duration_cast<nanoseconds>(delta);
  return ns.count();
}

void
exit_if_error(bool const condition, char const* message)
{
  if (condition) {
    ::fprintf(stderr, "error '%s' reason: '%s'\n", message, ::strerror(errno));
    std::exit(EXIT_FAILURE);
  }
}

} // namespace shared
