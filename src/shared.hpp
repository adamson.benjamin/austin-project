#include <cstdint>

/// Contains data structures and algorithms used by both the reader/writer applications.

/// Message as-defined by email.
struct Message {
  uint8_t  type;
  uint64_t send_t; // nanos_since_epoch
  char     payload[112];
};

/// Wrapper around the block of SharedMemory used to communicate between the two processes.
///
/// Inside the block of shared memory, the first byte is a bool flag that is treated as atomic using
/// atomic_ref, which the two processes use to take turns writing, then reading.
///
/// Immediately after the flag is space for a single Message struct. By taking turns for
/// reading/writing using an atomic flag the amout of time between writing the message in process 1
/// and reading the  message in process 2 is minimized.
class SharedMemory final
{
  int   id_;
  void *pmem_;
  void *pmessages_;
  bool *pflag_;

private:
  SharedMemory(int, void*);

  Message& message_ref();
  bool& flag_ref() const { return *pflag_; }

public:
  /// Return the shared memory ready to be used by both processes, or abort.
  static SharedMemory get_or_abort();
  ~SharedMemory();

  bool ready_for_read() const;
  bool ready_for_write() const;

  Message read_message();
  void    write_message(Message const&);
};

/// Contains common functions/constants to both processes.
namespace shared
{
auto constexpr NUM_MESSAGES = 1000000; // ONE MILLION PER INSTRUCTIONS

/// Exit and print an error message if condition is true.
void exit_if_error(bool, char const*);

/// Return a queue id that can be used by either process to send/recv messages from for
//communication between the two processes.
int msgqueue_key();

/// Get the current number of nanoseconds since epoch using the system clock.
uint64_t nanos_since_epoch();
} // namespace shared
