#include "shared.hpp"

#include <array>
#include <cstdlib>
#include <cstdio>
#include <iostream>


#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/shm.h>

#include <cstring>
#include <time.h>

namespace sh = ::shared;

namespace
{
inline void
send_msg(SharedMemory& sm)
{
  auto constexpr PAYLOADS = std::array{
    "PAYLOAD 0",
    "PAYLOAD 1",
    "PAYLOAD 2"
  };
  char const* payload = PAYLOADS[::rand() % PAYLOADS.size()];

  static auto count = 0;

  Message msg;
  ::strcpy(msg.payload, payload);
  msg.type      = count++;                 // This will overflow many times, just for debugging.
  msg.send_t    = sh::nanos_since_epoch();
  sm.write_message(msg);
}

/// This process will wait (block) until the reader process sends a message to this program,
/// indicating the process is ready to receive messages.
void
wait_for_reader_process()
{
  auto const qid = sh::msgqueue_key();

  Message msg;
  sh::exit_if_error(sizeof(msg) != ::msgrcv(qid, &msg, sizeof(msg), 0, 0), "msgrcv");
  sh::exit_if_error(0 != ::msgctl(qid, IPC_RMID, nullptr), "msgctl");
}

} // namespace

int
main(int argc, char **argv)
{
  ::fprintf(stdout, "WRITER -- starting\n");
  ::srand(::time(nullptr));

  ::fprintf(stdout, "WRITER -- waiting for message from reader process\n");
  wait_for_reader_process();

  ::fprintf(stdout, "WRITER -- writing all messages\n");
  {
    auto sm = SharedMemory::get_or_abort();
    for (auto i = 0; i < sh::NUM_MESSAGES; ++i) {
      while (!sm.ready_for_write()) {}
      send_msg(sm);
    }
  }
  ::fprintf(stdout, "WRITER -- finished sending %d messages to READER.\n", sh::NUM_MESSAGES);
  return EXIT_SUCCESS;
}
